// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)
package prj5;

/**
 * Survey takers' regions as enumerators
 * 
 * @author Sai saihhh
 * @version 11/19/19
 *
 */
public enum RegionEnum {
    NE_US, SE_US, THE_REST_OF_US, OUTSIDE_US;
}
