// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import list.ListInterface;

/**
 * Class that defines a basic implementation of the linked list with sorting
 * functionality.
 * 
 * @author Aman Kothari (amank)
 * @version 11/19/2019
 * @param <T>
 *            type of object that the linked list will store
 */
public class LinkedList<T> implements ListInterface<T> {

    /**
     * Class that defines a node to store data and point to the next data
     * object.
     *
     * @param <E>
     *            the type of the object data the node will store.
     */
    private static class Node<E> {

        private E data;
        private Node<E> next;


        /**
         * Constructor that initializes new Node object with data but no pointer
         * to the next data.
         *
         * @param data
         *            Object of type E that node will store
         */
        public Node(E data) {
            this(data, null);
        }


        /**
         * Constructor that initializes new Node object with data and a pointer
         * to the next data.
         *
         * @param data
         *            Object of type E that node will store
         * @param nextNode
         *            the node which the current node points to
         */
        public Node(E data, Node<E> nextNode) {
            this.data = data;
            next = nextNode;
        }


        /**
         * Method that returns the next node
         *
         * @return next node in the list
         */
        public Node<E> next() {
            return this.next;
        }


        /**
         * Method that returns data of current node
         *
         * @return data
         */
        public E getData() {
            return this.data;
        }


        /**
         * 
         * @param nextNode
         *            a
         */
        public void setNext(Node<E> nextNode) {
            this.next = nextNode;
        }

    }

    private Node<T> head;
    private int size;


    /**
     * Creates new LinkedList object and initializes fields to default
     * values.
     */
    public LinkedList() {
        head = null;
        size = 0;
    }


    /**
     * Adds entry to the back a list.
     * 
     * @param newEntry
     *            entry to be added
     * @throws IllegalArgumentException
     *             when entry passed is null
     */
    @Override
    public void add(T newEntry) {
        // check if the object is null
        if (newEntry == null) {
            throw new IllegalArgumentException("Object is null");
        }

        Node<T> current = head;

        if (isEmpty()) {
            head = new Node<T>(newEntry);
        }

        // other cases
        else {
            while (current.next != null) {
                current = current.next;
            }
            current.setNext(new Node<T>(newEntry));
        }
        size++;

    }


    /**
     * Adds entry at specified position in the list.
     * 
     * @param index
     *            position at which entry should be added
     * @param newEntry
     *            entry to be added to list
     * @throws IllegalArgumentException
     *             when entry passed is null
     * @throws IndexOutOfBoundsException
     *             when index passed is out of bounds of list
     */
    @Override
    public void add(int index, T newEntry) {
        if (newEntry == null) {
            throw new IllegalArgumentException("Object is null");
        }
        // check if the index is out of bounds
        if ((index < 0) || (index > getLength())) {
            throw new IndexOutOfBoundsException("Index is out of bounds");
        }
        Node<T> current = head;

        // empty stack case
        if (isEmpty()) {
            head = new Node<T>(newEntry);
        }
        else {
            if (index == 0) {
                Node<T> newNode = new Node<T>(newEntry);
                newNode.setNext(head);
                head = newNode;
            }
            else {
                int currentIndex = 0;
                while (current != null) {
                    if ((currentIndex + 1) == index) {
                        Node<T> nextNext = current.next;
                        Node<T> newNode = new Node<T>(newEntry);
                        current.setNext(newNode);
                        newNode.setNext(nextNext);

                    }
                    current = current.next();
                    currentIndex += 1;
                }
            }
        }
        size++;

    }


    /**
     * Clears the list and sets default values.
     */
    @Override
    public void clear() {
        // make sure we don't call clear on an empty list
        if (head != null) {
            head.setNext(null);
            head = null;
            size = 0;
        }

    }


    /**
     * Checks if list contains specified entry
     * 
     * @return boolean value indicating whether list contains
     *         specified item or not.
     * @param entry
     *            entry to be checked for in the list
     * @throws IllegalArgumentException
     *             when entry passed is null
     */
    @Override
    public boolean contains(T entry) {
        if (entry == null) {
            throw new IllegalArgumentException();
        }
        Node<T> current = head;
        while (current != null) {
            if (entry.equals(current.data)) {
                return true;
            }
            current = current.next;
        }

        return false;
    }


    /**
     * Returns entry at specified position.
     * 
     * @param index
     *            position of entry to be returned
     * @return entry at specified position
     * @throws IndexOutOfBoundsException
     *             when position specified is out of bounds of list
     */
    @Override
    public T getEntry(int index) {
        if (index > size - 1 || index < 0) {
            throw new IndexOutOfBoundsException("Index exceeds the size.");
        }
        Node<T> current = head;
        int currentIndex = 0;
        T data = null;
        while (current != null) {
            if (currentIndex == index) {
                data = current.data;
            }
            currentIndex++;
            current = current.next;
        }
        return data;
    }


    /**
     * Returns length of the list
     * 
     * @return integer value indicating list length
     */
    @Override
    public int getLength() {
        return size;
    }


    /**
     * Checks whether list is empty or not.
     * 
     * @return Boolean value indicating whether list is
     *         empty or not
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    /**
     * Removes entry at specified position from the list.
     * 
     * @return Entry that was removed
     * @param index
     *            position at which entry is to be removed
     * @throws IndexOutOfBoundException
     *             when position specified is out of list bounds
     */
    @Override
    public T remove(int index) {
        // if the index is invalid
        if (index < 0 || head == null) {
            throw new IndexOutOfBoundsException("Index is out of bounds");
        }
        else {
            Node<T> current = head;
            int currentIndex = 0;
            if (index == 0) {
                T data = head.data;
                head = head.next;
                size--;
                return data;
            }

            while (current.next != null) {
                if ((currentIndex + 1) == index) {
                    T data = current.next.getData();
                    Node<T> newNext = current.next.next;
                    current.setNext(newNext);
                    size--;
                    return data;
                }
                current = current.next;
                currentIndex++;
            }

            // if the element was never found, this also handles empty case
            throw new IndexOutOfBoundsException("Index is out of bounds");
        }
    }


    /**
     * Replaces entry at specified index with specified entry
     * 
     * @return Entry being replaced
     * @param index
     *            position of entry to be replaced
     * @param newEntry
     *            entry to be added to list
     */
    @Override
    public T replace(int index, T newEntry) {
        T data = this.remove(index);
        this.add(index, newEntry);
        return data;
    }


    /**
     * Returns an array containing the list entries
     * 
     * @return array containing list entries
     */
    @Override
    public Object[] toArray() {
        Object[] array = new Object[this.getLength()];

        Node<T> current = head;
        int count = 0;
        while (current != null) {
            array[count] = current.getData();
            current = current.next;
            count++;
        }

        return array;
    }


    /**
     * Inner private class that facilitates iterator functionality
     * for kist object.
     * 
     * @author Aman Kothari (amank)
     * @version 11/19/2019
     */
    private class LinkedListIterator<E> implements Iterator<E> {
        private Node<E> curr;


        /**
         * Creates a new LinkedList iterator object
         */
        @SuppressWarnings("unchecked")
        public LinkedListIterator() {
            curr = new Node<E>(null);
            curr.setNext((Node<E>)head);
        }


        /**
         * Checks whether there is another value in the list or not
         * 
         * @return boolean value indicating presence of next entry
         */
        @Override
        public boolean hasNext() {
            return curr.next() != null;
        }


        /**
         * iterator returns the next item in the list
         * 
         * @return next item in the list
         * @throws NoSuchElementException
         *             when no other item is present in the list
         */
        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            curr = curr.next();
            return curr.getData();
        }
    }


    /**
     * Method that creates an iterator for current list object
     * 
     * @return iterator for current list object
     */
    public Iterator<T> iterator() {
        return new LinkedListIterator<T>();
    }


    /**
     * Helper method to swap positions in the linked list
     * of two consecutive entries
     * 
     * @precondition index is in list bounds
     * 
     * @param index
     *            position where swapping is to take place
     */
    private void swap(int index) {
        T tmp = replace(index, getEntry(index - 1));
        replace(index - 1, tmp);
    }


    /**
     * Method that provides sort functionality to list object.
     * 
     * @param comparator
     *            comparator object to facilitate sorting
     * 
     */
    public void sort(Comparator<? super T> comparator) {
        for (int i = 1; i < getLength(); i++) {
            for (int j = i; (j > 0) && comparator.compare(getEntry(j), getEntry(
                j - 1)) < 0; j--) {
                swap(j);
            }
        }
    }

}
