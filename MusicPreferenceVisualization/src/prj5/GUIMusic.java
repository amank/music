// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import java.awt.Color;
import CS2114.Button;
import CS2114.Shape;
import CS2114.TextShape;
import CS2114.Window;
import CS2114.WindowSide;

/**
 * @author Aman Kothari (amank)
 * @version 12/03/19
 */
public class GUIMusic {

    private Window window;
    private MusicCalculator calc;
    private Button[] buttons;
    private int enumChoice;
    private int page;

    public static final int WINDOW_WIDTH = 1300;
    public static final int WINDOW_HEIGHT = 700;
    public static final int[] BAR_DIMENSIONS = { 8, 80 };
    private final int[] GLYPH_WIDTH = { 250, 650, 1050 };
    private final int[] GLYPH_HEIGHT = { 50, 200, 350 };


    /**
     * Creates a new GUIMusic object that initializes the front end window.
     * Initializes fields, sets all default values, and adds functionality
     * buttons to the window.
     * 
     * @param calculator
     *            calculator to do all back-end calculations
     */
    public GUIMusic(MusicCalculator calculator) {
        window = new Window("Project 5");
        window.setSize(WINDOW_WIDTH + 100, WINDOW_HEIGHT);

        calc = calculator;
        enumChoice = 1;
        page = 0;

        buttons = new Button[10];

        // top side buttons
        buttons[0] = new Button("prev");
        buttons[0].onClick(this, "clickedPrev");
        buttons[1] = new Button("Sort by Artist Name");
        buttons[1].onClick(this, "clickedArtistSort");
        buttons[2] = new Button("Sort by Song Title");
        buttons[2].onClick(this, "clickedTitleSort");
        buttons[3] = new Button("Sort by Release Year");
        buttons[3].onClick(this, "clickedYearSort");
        buttons[4] = new Button("Sort by Genre");
        buttons[4].onClick(this, "clickedGenreSort");
        buttons[5] = new Button("Next");
        buttons[5].onClick(this, "clickedNext");
        for (int i = 0; i < 6; i++) {
            window.addButton(buttons[i], WindowSide.NORTH);
        }

        // bottom side buttons
        buttons[6] = new Button("Represent Hobby");
        buttons[6].onClick(this, "clickedHobby");
        buttons[7] = new Button("Represent Major");
        buttons[7].onClick(this, "clickedMajor");
        buttons[8] = new Button("Represent Region");
        buttons[8].onClick(this, "clickedRegion");
        buttons[9] = new Button("Quit");
        buttons[9].onClick(this, "clickedQuit");
        for (int i = 6; i < 10; i++) {
            window.addButton(buttons[i], WindowSide.SOUTH);
        }

        // disable next and prev initially
        buttons[0].disable();
        buttons[5].disable();
    }


    /**
     * Method defines functionality when 'Quit' button is clicked.
     * 
     * @param button
     *            button when clicked, method is executed
     * 
     */
    public void clickedQuit(Button button) {
        System.exit(0);
    }


    /**
     * method defines functionality when 'Next' button is clicked.
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedNext(Button button) {
        page++;
        update();
    }


    /**
     * method defines functionality when 'Prev' button is clicked
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedPrev(Button button) {
        page--;
        update();
    }


    /**
     * method defines functionality when 'Sort by Artist Name'
     * button is clicked
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedArtistSort(Button button) {
        calc.getSongList().sortByArtist();
        update();
    }


    /**
     * method defines functionality when 'Sort by Song Title'
     * button is clicked
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedTitleSort(Button button) {
        calc.getSongList().sortByTitle();
        update();
    }


    /**
     * method defines functionality when 'Sort by Release Year'
     * button is clicked
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedYearSort(Button button) {
        calc.getSongList().sortByYear();
        update();
    }


    /**
     * method defines functionality when 'Sort by Genre'
     * button is clicked
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedGenreSort(Button button) {
        calc.getSongList().sortByGenre();
        update();
    }


    /**
     * method defines functionality when 'Represent Hobby'
     * button is clicked
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedHobby(Button button) {
        enumChoice = 1;
        update();
    }


    /**
     * method defines functionality when 'Represent Major'
     * button is clicked
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedMajor(Button button) {
        enumChoice = 2;
        update();
    }


    /**
     * method defines functionality when 'Represent Region'
     * button is clicked
     * 
     * @param button
     *            button when clicked, method is executed
     */
    public void clickedRegion(Button button) {
        enumChoice = 3;
        update();
    }


    /**
     * Method to update window every time a new button is
     * clicked.
     */
    private void update() {
        // enable and/or disable next and prev buttons
        if (page == 0) {
            buttons[0].disable();
        }
        else {
            buttons[0].enable();
        }
        if (page == (calc.getSongList().getLength() - 1) / 9) {
            buttons[5].disable();
        }
        else {
            buttons[5].enable();
        }
        // adds songs and legend to the window
        addAllSongs();
        addLegendToWindow();
    }


    /**
     * Adds 9 song glyphs to the window depending on button clicked
     * and sorted lists.
     */
    private void addAllSongs() {
        window.removeAllShapes();
        for (int i = 9 * page; (i < 9 * page + 9) && (i < calc.getSongList()
            .getLength()); i++) {
            addSongToWindow(calc.getSongList().getEntry(i), GLYPH_WIDTH[i % 3],
                GLYPH_HEIGHT[(i % 9) / 3]);
        }
    }


    /**
     * Method adds an individual song the window with associated data and
     * visual representations at specified location on the window
     * 
     * @param song
     *            song to be added to window
     * @param width
     *            width at which song data should be added onto the window
     * @param height
     *            height at which song should be added onto the window
     */
    private void addSongToWindow(Song song, int width, int height) {
        //adds song title
        TextShape textTitle = new TextShape(0, 0, song.getTitle());
        textTitle.setX(width - textTitle.getWidth() / 2);
        textTitle.setY(height - textTitle.getHeight() / 2);
        textTitle.setBackgroundColor(Color.WHITE);
        window.addShape(textTitle);
        //adds song artist
        TextShape textArtist = new TextShape(0, 0, "by " + song.getArtist());
        textArtist.setX(width - textArtist.getWidth() / 2);
        textArtist.setY(textTitle.getY() + textTitle.getHeight());
        textArtist.setBackgroundColor(Color.WHITE);
        window.addShape(textArtist);

        //adds bar to separate heard and likes percentage bars.
        Shape bar = new Shape(width, height + 30, BAR_DIMENSIONS[0],
            BAR_DIMENSIONS[1], Color.BLACK);
        window.addShape(bar);
        
        //calculates percentages based on choice
        int[] data;
        switch (enumChoice) {
            case 1:
                data = calc.calculateHobbyForSong(song.getId());
                break;
            case 2:
                data = calc.calculateMajorForSong(song.getId());
                break;
            default:
                data = calc.calculateRegionForSong(song.getId());
                break;
        }

        //adds percentage bars in appropriate colors to window
        Shape[] preferences = new Shape[8];
        preferences[0] = new Shape(width - 3 * data[0] / 2, height + 30, 3
            * data[0] / 2, 20, Color.MAGENTA);
        preferences[1] = new Shape(width + bar.getWidth() / 2, height + 30, 3
            * data[1] / 2, 20, Color.MAGENTA);
        preferences[2] = new Shape(width - 3 * data[2] / 2, height + 50, 3
            * data[2] / 2, 20, Color.BLUE);
        preferences[3] = new Shape(width + bar.getWidth() / 2, height + 50, 3
            * data[3] / 2, 20, Color.BLUE);
        preferences[4] = new Shape(width - 3 * data[4] / 2, height + 70, 3
            * data[4] / 2, 20, Color.ORANGE);
        preferences[5] = new Shape(width + bar.getWidth() / 2, height + 70, 3
            * data[5] / 2, 20, Color.ORANGE);
        preferences[6] = new Shape(width - 3 * data[6] / 2, height + 90, 3
            * data[6] / 2, 20, Color.GREEN);
        preferences[7] = new Shape(width + bar.getWidth() / 2, height + 90, 3
            * data[7] / 2, 20, Color.GREEN);
        for (int i = 0; i < 8; i++) {
            window.addShape(preferences[i]);
        }
    }

    /**
     * Helper method to add the legend box onto the window
     */
    private void addLegendToWindow() {
        Shape legend = new Shape(WINDOW_WIDTH - 90, WINDOW_HEIGHT / 3, 150, 250,
            Color.WHITE);
        legend.setForegroundColor(Color.BLACK);

        TextShape[] category = new TextShape[5];
        category[0] = new TextShape(0, 0, "");
        category[1] = new TextShape(0, 0, "", Color.MAGENTA);
        category[2] = new TextShape(0, 0, "", Color.BLUE);
        category[3] = new TextShape(0, 0, "", Color.ORANGE);
        category[4] = new TextShape(0, 0, "", Color.GREEN);

        switch (enumChoice) {
            case 1:
                category[0].setText("Hobby Legend");
                category[1].setText("Read");
                category[2].setText("Art");
                category[3].setText("Sports");
                category[4].setText("Music");
                break;
            case 2:
                category[0].setText("Major Legend");
                category[1].setText("Comp Sci");
                category[2].setText("Other Eng");
                category[3].setText("Math/CMDA");
                category[4].setText("Other");
                break;
            default:
                category[0].setText("Region Legend");
                category[1].setText("Northeast US");
                category[2].setText("SouthEast US");
                category[3].setText("Rest of US");
                category[4].setText("Outside US");
                break;
        }
        for (int i = 0; i < 5; i++) {
            category[i].setBackgroundColor(Color.WHITE);
            category[i].setX(legend.getX() + 20);
            category[i].setY(legend.getY() + 20 * (i + 1));
            window.addShape(category[i]);
        }
        TextShape[] representation = new TextShape[3];
        representation[0] = new TextShape(legend.getX() + 30, legend.getY()
            + 140, "Song Title");
        representation[1] = new TextShape(legend.getX() + 10, legend.getY()
            + 190, "Heard");
        representation[2] = new TextShape(legend.getX() + 80, legend.getY()
            + 190, "Likes");
        for (int i = 0; i < 3; i++) {
            representation[i].setBackgroundColor(Color.WHITE);
            window.addShape(representation[i]);
        }

        Shape bar = new Shape(legend.getX() + 65, legend.getY() + 170, 4, 60,
            Color.BLACK);
        window.addShape(bar);
        window.addShape(legend);
    }

}
