// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Class that reads given data and stores it in an appropriate
 * memory structure.
 * 
 * @author Aman Kothari (amank)
 * @version 11/19/19
 */
public class FileReader {
    private LinkedList<Person> people;
    private SongList songs;
    private MusicCalculator calc;
    @SuppressWarnings("unused")
    private GUIMusic music;


    /**
     * Constructor to create new FileReader() object.
     * Initializes a new window to display the given data.
     * 
     * @param peopleList
     *            people list file name
     * @param songList
     *            song list file name
     * @throws FileNotFoundException
     *             when either file not found
     */
    public FileReader(String songList, String poepleList)
        throws FileNotFoundException {
        songs = readSongFile(songList);
        people = readPeopleFile(poepleList);
        calc = new MusicCalculator(songs, people);
        music = new GUIMusic(calc);
        display();
    }


    /**
     * Reads the file with input for each individual's credentials
     * and responses to music survey. Stores each person as objects
     * in a linked list.
     * 
     * @param fileName
     *            file name containing people information
     * @return LinkedList object with person objects for each valid
     *         person input
     * @throws FileNotFoundException
     *             thrown when file is not found
     */
    private LinkedList<Person> readPeopleFile(String fileName)
        throws FileNotFoundException {
        LinkedList<Person> people = new LinkedList<>();

        Scanner scanner = new Scanner(new File(fileName));
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String[] credentials = scanner.nextLine().split(",");
            int[] responses = new int[2 * songs.getLength()];
            int id = Integer.parseInt(credentials[0]);
            MajorEnum major = getMajor(credentials[2]);
            RegionEnum region = getRegion(credentials[3]);
            HobbyEnum hobby = getHobby(credentials[4]);
            for (int i = 0; i < 2 * songs.getLength(); i++) {
                if (5 + i <= credentials.length - 1) {
                    responses[i] = getResponse(credentials[5 + i]);
                }
                else {
                    responses[i] = -1;
                }

            }
            if (hobby != null && major != null && region != null) {
                Person newEntry = new Person(id, hobby, major, region,
                    responses);
                people.add(newEntry);
            }

        }
        scanner.close();
        return people;
    }


    /**
     * Identify hobby for person and assign it appropriate enum choice.
     * 
     * @param hobby
     *            string value for person's hobby
     * @return HobbyEnum for person's hobby preference.
     */
    private HobbyEnum getHobby(String hobby) {
        switch (hobby.toLowerCase()) {
            case "reading":
                return HobbyEnum.READ;
            case "sports":
                return HobbyEnum.SPORTS;
            case "music":
                return HobbyEnum.MUSIC;
            case "art":
                return HobbyEnum.ART;
            default:
                return null;
        }

    }


    /**
     * Identify major for person and assign it appropriate enum choice.
     * 
     * @param major
     *            string value for person's major
     * @return MajorEnum for person's major preference.
     */
    private MajorEnum getMajor(String major) {
        switch (major.toLowerCase()) {
            case "computer science":
                return MajorEnum.CS;
            case "math or cmda":
                return MajorEnum.MATH_CMDA;
            case "other engineering":
                return MajorEnum.ENGINEERING;
            case "other":
                return MajorEnum.OTHER;
            default:
                return null;
        }
    }


    /**
     * Identify region for person and assign it appropriate enum choice.
     * 
     * @param region
     *            string value for person's region
     * @return HobbyEnum for person's region information.
     */
    private RegionEnum getRegion(String region) {
        switch (region) {
            case "Southeast":
                return RegionEnum.SE_US;
            case "Northeast":
                return RegionEnum.NE_US;
            case "Outside of United States":
                return RegionEnum.OUTSIDE_US;
            case "United States (other than Southeast or Northwest)":
                return RegionEnum.THE_REST_OF_US;
            default:
                return null;
        }
    }


    /**
     * Helper method to identify response and assign
     * appropriate integer for choice.
     * 1 - Yes
     * 0 - No
     * -1 - no response
     * 
     * @param response
     *            response to survey question
     * @return integer value indicating response
     */
    private int getResponse(String response) {
        response = response.toLowerCase();
        switch (response) {
            case "yes":
                return 1;
            case "no":
                return 0;
            default:
                return -1;
        }
    }


    /**
     * Method to read the song list file. Reads songs and stores them
     * in a SongList object .
     * 
     * @param songList
     *            song list file name
     * @return SongList object containing Songs objects
     * @throws FileNotFoundException
     *             when file not found
     */
    private SongList readSongFile(String songList)
        throws FileNotFoundException {
        SongList tempList = new SongList();

        int songCount = 0;
        Scanner scanner = new Scanner(new File(songList));
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String[] credentials = scanner.nextLine().split(",");
            int year = Integer.parseInt(credentials[2]);
            Song newEntry = new Song(credentials[1], credentials[0], year,
                credentials[3], songCount);
            tempList.add(newEntry);
            songCount++;
        }
        scanner.close();

        return tempList;
    }


    /**
     * Method to display sorted list and corresponding heard and like
     * percentages for reference test.
     */
    private void display() {
        SongList songListCopy = calc.getSongList();
        songListCopy.sortByGenre();
        Iterator<Song> iter = songListCopy.iterator();
        while (iter.hasNext()) {

            Song curr = iter.next();
            int[] per = calc.calculateHobbyForSong(curr.getId());
            System.out.println(curr.toString());
            System.out.println(displaySong(per));

        }
        songListCopy.sortByTitle();
        iter = songListCopy.iterator();
        while (iter.hasNext()) {

            Song curr = iter.next();
            int[] per = calc.calculateHobbyForSong(curr.getId());
            System.out.println(curr.toString());
            System.out.println(displaySong(per));
        }

    }


    /**
     * Helper method to display song percentages.
     * 
     * @param per
     *            integer array conataining percentages to be printed.
     * @return String value of displayed percentages.
     */
    private String displaySong(int[] per) {
        StringBuilder builder = new StringBuilder();
        builder.append("Heard");
        builder.append("\nreading:");
        builder.append(per[0]);
        builder.append(" art:");
        builder.append(per[2]);
        builder.append(" sports:");
        builder.append(per[4]);
        builder.append(" music:");
        builder.append(per[6]);
        builder.append("\nLikes");
        builder.append("\nreading:");
        builder.append(per[1]);
        builder.append(" art:");
        builder.append(per[3]);
        builder.append(" sports:");
        builder.append(per[5]);
        builder.append(" music:");
        builder.append(per[7]);
        builder.append("\n");
        return builder.toString();
    }

}
