// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)
package prj5;

import student.TestCase;

/**
 * @author Sai saihhh
 * @version 2019.11.19
 *
 */
public class SongTest extends TestCase {
    private Song a;
    private Song b;
    private CompareByTitle title;
    private CompareByGenre genre;


    /**
    * Creates two songs
    */
    public void setUp() {
        a = new Song("Flo Rida", "My House", 2017, "Trash", 0);
        b = new Song("Green Day", "Longview", 1994, "Punk Rock", 1);
        title = new CompareByTitle();
        genre = new CompareByGenre();
    }


    /**
    * Tests the whole Song class
    */
    public void testSong() {
        assertEquals("Flo Rida", a.getArtist());
        assertEquals("My House", a.getTitle());
        assertEquals(2017, a.getYear());
        assertEquals("Trash", a.getGenre());
    }


    /**
    * Tests the CompareByArtist class
    * and CompareByYear class
    */
    public void testCompare() {
        assertEquals(1, title.compare(a, b));
        assertEquals(4, genre.compare(a, b));
        
        
        CompareByArtist artist = new CompareByArtist();
        CompareByYear year = new CompareByYear();
        
        assertEquals(-1, artist.compare(a, b));
        assertEquals(23, year.compare(a, b));
    }


    /**
    * Tests the String representation of a song
    */
    public void testToString() {
        assertEquals("Song Title: Longview\nSong Artist: " + "Green Day\nSong "
            + "Genre: Punk Rock\nSong Year: 1994", b.toString());
    }
}
