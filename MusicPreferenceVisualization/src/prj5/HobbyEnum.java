// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)
package prj5;

/**
 * Survey takers' hobbies as enumerators
 * 
 * @author Sai saihhh
 * @version 11/19/2019
 *
 */
public enum HobbyEnum {
    ART, MUSIC, SPORTS, READ;
}
