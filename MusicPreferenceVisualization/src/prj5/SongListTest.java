// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import student.TestCase;

/**
 * @author Aman Kothari (amank)
 * @version 2019.11.19
 */
public class SongListTest extends TestCase {
    private SongList list;


    /**
    * Creates a list of songs
    */
    public void setUp() {
        list = new SongList();
        list.add(new Song("Flo Rida", "My House", 2017, "Trash", 0));
        list.add(new Song("Green Day", "Longview", 1994, "Punk Rock", 1));
        list.add(new Song("John Denver", "Take Me Home", 1971, "Country", 2));
        list.add(new Song("Queen", "Radio Gaga", 1984, "Rock", 3));
        list.add(new Song("Aerosmith", "Dream On", 1964, "Rock", 4));
    }


    /**
    * Tests sortByArtist()
    */
    public void testSortByArtist() {
        list.sortByArtist();
        assertEquals("Aerosmith", list.getEntry(0).getArtist());
        assertEquals("Flo Rida", list.getEntry(1).getArtist());
        assertEquals("Green Day", list.getEntry(2).getArtist());
    }


    /**
    * Tests sortByTitle()
    */
    public void testSortByTitle() {
        list.sortByTitle();
        assertEquals("Dream On", list.getEntry(0).getTitle());
        assertEquals("Longview", list.getEntry(1).getTitle());
        assertEquals("My House", list.getEntry(2).getTitle());
        assertEquals("Radio Gaga", list.getEntry(3).getTitle());
        assertEquals("Take Me Home", list.getEntry(4).getTitle());

    }


    /**
    * Tests sortByGenre()
    */
    public void testSortByGenre() {
        list.sortByGenre();
        assertEquals("Country", list.getEntry(0).getGenre());
        assertEquals("Punk Rock", list.getEntry(1).getGenre());
        assertEquals("Rock", list.getEntry(2).getGenre());
        assertEquals("Rock", list.getEntry(3).getGenre());
        assertEquals("Trash", list.getEntry(4).getGenre());
    }
    
    /**
    * Tests sortByYear()
    */
    public void testSortByYear() {
        list.sortByYear();
        assertEquals(1964, list.getEntry(0).getYear());
        assertEquals(1971, list.getEntry(1).getYear());
        assertEquals(1984, list.getEntry(2).getYear());
        assertEquals(1994, list.getEntry(3).getYear());
        assertEquals(2017, list.getEntry(4).getYear());
    }
}
