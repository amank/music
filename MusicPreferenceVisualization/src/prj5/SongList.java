// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

/**
 * Class that extends LinkedList and provides sorting functionality
 * to songs based on artist name, song title, song genre, and
 * song year
 * 
 * @author Aman Kothari (amank)
 * @version 11/19/19
 */
public class SongList extends LinkedList<Song> {

    /**
     * Creates a new SongList object.
     */
    public SongList() {
        super();
    }


    /**
     * Sorts songs in SongList object by artist name
     */
    public void sortByArtist() {
        CompareByArtist artist = new CompareByArtist();
        super.sort(artist);
    }


    /**
     * Sorts songs in SongList object by song title
     */
    public void sortByTitle() {
        CompareByTitle title = new CompareByTitle();
        super.sort(title);
    }


    /**
     * Sorts songs in SongList object by song genre
     */
    public void sortByGenre() {
        CompareByGenre genre = new CompareByGenre();
        super.sort(genre);
    }

    /**
     * Sorts songs in SongList object by song year
     */
    public void sortByYear() {
        CompareByYear year = new CompareByYear();
        super.sort(year);
    }
}
