// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)
package prj5;

/**
 * Class that creates a song object and stores its credentials
 * including title,artist name, year, and genre.
 * 
 * @author Sai (saihhh)
 * @version 11/19/19
 */
public class Song {
    private int id;
    private String artist;
    private String title;
    private int year;
    private String genre;


    /**
     * Creates new Song object and initializes its fields.
     * 
     * @param artist
     *            String value of artist name
     * @param title
     *            String value of song title
     * @param year
     *            integer value of song year
     * @param genre
     *            String value of song genre
     * @param id
     *            song identification number
     */
    public Song(String artist, String title, int year, String genre, int id) {
        this.artist = artist;
        this.title = title;
        this.year = year;
        this.genre = genre;
        this.id = id;
    }


    /**
     * Returns song's artist name
     * 
     * @return String value of artist name
     */
    public String getArtist() {
        return artist;
    }


    /**
     * Returns song's title
     * 
     * @return String value of song title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Returns song's year
     * 
     * @return integer value of song year
     */
    public int getYear() {
        return year;
    }


    /**
     * Returns song's genre
     * 
     * @return String value of song genre
     */
    public String getGenre() {
        return genre;
    }


    /**
     * Returns song's identification number
     * 
     * @return integer value of songs's identification number
     */
    public int getId() {
        return id;
    }


    /**
     * Returns song's credentials as a string
     * 
     * @return String value of song's credentials.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Song Title: ");
        builder.append(title);
        builder.append("\n");
        builder.append("Song Artist: ");
        builder.append(artist);
        builder.append("\n");
        builder.append("Song Genre: ");
        builder.append(genre);
        builder.append("\n");
        builder.append("Song Year: ");
        builder.append(year);
        return builder.toString();
    }

}
