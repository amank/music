// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import java.util.Comparator;

/**
 * @author Aman Kothari (amank)
 * @version 2019.12.2
 */
public class CompareByYear implements Comparator<Song> {
    /**
     * Compares the songs by title
     * 
     * @param song0
     *            first song
     * @param song1
     *            second song
     * @return Negative integer if the first song was released before 
     *          the second
     *         1 if it was released after
     *         0 if they were released in the same year
     */
    @Override
    public int compare(Song song0, Song song1) {
        return song0.getYear() - song1.getYear();
    }
}
