// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)
package prj5;

import java.util.Comparator;

/**
 * @author Sai saihhh
 * @version 11/11/11
 *
 */
public class CompareByGenre implements Comparator<Song> {
    /**
     * Compares the songs by genre
     * 
     * @param song0
     *            first song
     * @param song1
     *            second song
     * @return Negative integer if the genre of the first song comes before
     *         that of the second song when sorted alphabetically
     *         Positive integer if it comes after
     *         0 if they are equal
     */
    @Override
    public int compare(Song song0, Song song1) {
        return song0.getGenre().compareToIgnoreCase(song1.getGenre());
    }

}
