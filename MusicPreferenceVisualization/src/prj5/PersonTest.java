// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)
package prj5;

import student.TestCase;

/**
 * @author Sai saihhh
 * @version 2019.11.19
 *
 */
public class PersonTest extends TestCase {
    private Person a;
    private int[] responses1 = { 1, 2, 3 };


    /**
    * Sets up the initial conditions
    */
    public void setUp() {
        a = new Person(456, HobbyEnum.MUSIC, MajorEnum.OTHER,
            RegionEnum.OUTSIDE_US, responses1);
    }


    /**
    * Tests all of the methods of the Person class
    */
    public void testPerson() {
        assertEquals(456, a.getID());
        assertEquals(HobbyEnum.MUSIC, a.getHobby());
        assertEquals(MajorEnum.OTHER, a.getMajor());
        assertEquals(RegionEnum.OUTSIDE_US, a.getRegion());
        for (int i = 1; i < 4; i++) {
            assertEquals(i, responses1[i - 1]);
        }
    }


    /**
    * Tests the toString() method
    */
    public void testToString() {
        assertEquals("[456, MUSIC, OTHER, OUTSIDE_US]", a.toString());
    }
}
