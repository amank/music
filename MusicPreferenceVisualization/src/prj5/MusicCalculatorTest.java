// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import student.TestCase;

/**
 * @author Aman Kothari (amank)
 * @version 2019.11.19
 */
public class MusicCalculatorTest extends TestCase {
    private MusicCalculator calc;
    private LinkedList<Person> people;


    /**
     * Sets up the initial conditions
     * Creates a song list containing the songs from the sample files
     */
    public void setUp() {
        SongList songs = new SongList();
        songs.add(new Song("Metallica", "FTB", 1968, "Pop", 0));
        songs.add(new Song("City of Stars", "LLL", 1991, "Grunge", 1));
        songs.add(new Song("Guns n Roses", "November Rain", 2018, "Hip-Hop",
            2));
        songs.add(new Song("Led Zeppelin", "Stairway to Heaven", 2018,
            "Hip-Hop", 2));
        songs.add(new Song("Queen", "Bo Rhap", 2018, "Hip-Hop", 2));
        songs.add(new Song("Queen", "DSMN", 2018, "Hip-Hop", 2));
        songs.add(new Song("Elton John", "TD", 2018, "Hip-Hop", 2));
        songs.add(new Song("Billy Joel", "PM", 2018, "Hip-Hop", 2));
        songs.add(new Song("The Doors", "People are Strange", 2018, "Hip-Hop",
            2));

        people = new LinkedList<Person>();
        people.add(new Person(7, HobbyEnum.SPORTS, MajorEnum.ENGINEERING,
            RegionEnum.THE_REST_OF_US, new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, -1 }));

        people.add(new Person(8, HobbyEnum.MUSIC, MajorEnum.OTHER,
            RegionEnum.OUTSIDE_US, new int[] { -1, -1, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }));

        people.add(new Person(9, HobbyEnum.MUSIC, MajorEnum.OTHER,
            RegionEnum.OUTSIDE_US, new int[] { 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 1, 1, 1, -1 }));

        people.add(new Person(14, HobbyEnum.ART, MajorEnum.MATH_CMDA,
            RegionEnum.SE_US, new int[] { 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                0, 0, 0, 0, -1 }));

        people.add(new Person(17, HobbyEnum.READ, MajorEnum.CS,
            RegionEnum.NE_US, new int[] { 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                0, 0, 0, 0, -1 }));

        calc = new MusicCalculator(songs, people);
    }


    /**
     * Checks the output containing percentages for
     * each hobby. Also takes into consideration when
     * the answer to "heard" is valid and that to
     * "like" is invalid for each song.
     */
    public void testCalculateHobbyForSong() {
        int[] record2 = calc.calculateHobbyForSong(2);

        assertEquals(100, record2[0]);
        assertEquals(100, record2[1]);
        assertEquals(100, record2[2]);
        assertEquals(100, record2[3]);
        assertEquals(100, record2[4]);
        assertEquals(100, record2[5]);
        assertEquals(0, record2[6]);
        assertEquals(0, record2[7]);

        int[] record8 = calc.calculateHobbyForSong(8);
        assertEquals(0, record8[0]);
        assertEquals(0, record8[1]);
        assertEquals(0, record8[2]);
        assertEquals(0, record8[3]);
        assertEquals(100, record8[4]);
        assertEquals(0, record8[5]);
        assertEquals(100, record8[6]);
        assertEquals(0, record8[7]);

        people.remove(4);
        int[] record2modified = calc.calculateHobbyForSong(2);
        assertEquals(0, record2modified[0]);
        assertEquals(0, record2modified[1]);
        assertEquals(100, record2modified[2]);
        assertEquals(100, record2modified[3]);
        assertEquals(100, record2modified[4]);
        assertEquals(100, record2modified[5]);
        assertEquals(0, record2modified[6]);
        assertEquals(0, record2modified[7]);

        LinkedList<Person> personList = calc.getPersonList();
        assertEquals(4, personList.getLength());
        assertEquals(7, personList.getEntry(0).getID());
        assertEquals(14, personList.getEntry(3).getID());

        SongList songList = calc.getSongList();
        assertEquals(9, songList.getLength());
    }


    /**
     * Checks the output containing percentages for
     * each region. Also takes into consideration when
     * the answer to "heard" is valid and that to
     * "like" is invalid for each song.
     */
    public void testCalculateRegionForSong() {
        int[] record2 = calc.calculateRegionForSong(2);

        assertEquals(100, record2[0]);
        assertEquals(100, record2[1]);
        assertEquals(100, record2[2]);
        assertEquals(100, record2[3]);
        assertEquals(100, record2[4]);
        assertEquals(100, record2[5]);
        assertEquals(0, record2[6]);
        assertEquals(0, record2[7]);

        int[] record8 = calc.calculateRegionForSong(8);
        assertEquals(0, record8[0]);
        assertEquals(0, record8[1]);
        assertEquals(0, record8[2]);
        assertEquals(0, record8[3]);
        assertEquals(100, record8[4]);
        assertEquals(0, record8[5]);
        assertEquals(100, record8[6]);
        assertEquals(0, record8[7]);

        people.remove(4);
        int[] record2modified = calc.calculateRegionForSong(2);
        assertEquals(0, record2modified[0]);
        assertEquals(0, record2modified[1]);
        assertEquals(100, record2modified[2]);
        assertEquals(100, record2modified[3]);
        assertEquals(100, record2modified[4]);
        assertEquals(100, record2modified[5]);
        assertEquals(0, record2modified[6]);
        assertEquals(0, record2modified[7]);
    }


    /**
     * Checks the output containing percentages for
     * each major. Also takes into consideration when
     * the answer to "heard" is valid and that to
     * "like" is invalid for each song.
     */
    public void testCalculateMajorForSong() {
        int[] record2 = calc.calculateMajorForSong(2);

        assertEquals(100, record2[0]);
        assertEquals(100, record2[1]);
        assertEquals(100, record2[2]);
        assertEquals(100, record2[3]);
        assertEquals(100, record2[4]);
        assertEquals(100, record2[5]);
        assertEquals(0, record2[6]);
        assertEquals(0, record2[7]);

        int[] record8 = calc.calculateMajorForSong(8);
        assertEquals(0, record8[0]);
        assertEquals(0, record8[1]);
        assertEquals(0, record8[2]);
        assertEquals(0, record8[3]);
        assertEquals(100, record8[4]);
        assertEquals(0, record8[5]);
        assertEquals(100, record8[6]);
        assertEquals(0, record8[7]);

        people.remove(4);
        int[] record2modified = calc.calculateMajorForSong(2);
        assertEquals(0, record2modified[0]);
        assertEquals(0, record2modified[1]);
        assertEquals(100, record2modified[2]);
        assertEquals(100, record2modified[3]);
        assertEquals(100, record2modified[4]);
        assertEquals(100, record2modified[5]);
        assertEquals(0, record2modified[6]);
        assertEquals(0, record2modified[7]);
    }
}
