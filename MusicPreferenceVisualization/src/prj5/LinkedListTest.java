// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import java.util.Iterator;
import student.TestCase;

/**
 * @author Aman Kothari (amank)
 * @version 11/11/11
 */
public class LinkedListTest extends TestCase {

    private LinkedList<String> list;
    private String testString = "test";


    /**
    *  Sets up the initial condition for test cases
    */
    public void setUp() {
        list = new LinkedList<String>();
    }


    /**
    *   Tests the getLength() method
    */
    public void testGetLength() {
        assertEquals(0, list.getLength());
        list.add(testString);
        assertEquals(1, list.getLength());
    }


    /**
    *   Tests the isEmpty() method
    */
    public void testIsEmpty() {
        assertTrue(list.isEmpty());
        list.add(testString);
        assertFalse(list.isEmpty());
    }


    /**
    *   Tests the add(T) method
    */
    public void testAddNoIndex() {
        Exception thrown = null;
        try {
            list.add(null);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
        assertEquals(0, list.getLength());
        assertTrue(list.isEmpty());
        list.add(testString);
        assertFalse(list.isEmpty());
        assertEquals(1, list.getLength());
        list.add("B");
        assertEquals(2, list.getLength());
        assertEquals("B", list.getEntry(1));
    }


    /**
    *   Tests the add(int, T) method with invalid index values
    *   to check for the exception(s) thrown
    */
    public void testAddIndexExceptions() {
        Exception thrown = null;
        try {
            list.add(0, null);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
        thrown = null;
        try {
            list.add(-1, testString);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
        thrown = null;
        try {
            list.add(1, testString);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
    }


    /**
    *   Tests the add(int, T) method with valid index values
    */
    public void testAddIndex() {
        assertTrue(list.isEmpty());
        list.add(0, testString);
        assertFalse(list.isEmpty());
        assertEquals(1, list.getLength());
        assertEquals(testString, list.getEntry(0));
        list.add(0, "A");
        assertEquals("A", list.getEntry(0));
        assertEquals(2, list.getLength());
        list.add(2, "D");
        assertEquals("D", list.getEntry(2));
        list.add(2, "C");
        assertEquals("C", list.getEntry(2));
        assertEquals("D", list.getEntry(3));
    }


    /**
    *   Tests clear() method
    */
    public void testClear() {
        list.clear();
        list.add("A");
        list.clear();
        assertEquals(0, list.getLength());
        assertFalse(list.contains("A"));
    }


    /**
    * Tests getEntry(int) and makes sure
    * exceptions are thrown when the index is invalid
    */
    public void testGetEntry() {
        list.add(testString);
        Exception thrown = null;
        try {
            list.getEntry(-1);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
        thrown = null;
        try {
            list.getEntry(1);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
        list.add("A");
        assertEquals(testString, list.getEntry(0));
        assertEquals("A", list.getEntry(1));
    }


    /**
    * Tests the remove(int) method with invalid index values
    *   to check for the exception(s) thrown
    */
    public void testRemoveException() {
        Exception thrown = null;
        try {
            list.remove(0);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
        list.add(testString);
        thrown = null;
        try {
            list.remove(-1);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
        thrown = null;
        try {
            list.remove(1);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
    }


    /**
    * Tests remove(int) with valid index values
    */
    public void testRemove() {
        list.add("A");
        list.add("B");
        assertEquals("B", list.remove(1));
        assertEquals(1, list.getLength());
        list.add("B");
        assertEquals("A", list.remove(0));
        assertEquals(1, list.getLength());
        assertEquals("B", list.getEntry(0));
    }


    /**
    * Tests the replace(T) method
    */
    public void testReplace() {
        list.add("A");
        list.add("B");
        list.replace(0, "B");
        list.replace(1, "A");
        assertEquals("B", list.getEntry(0));
        assertEquals("A", list.getEntry(1));
    }


    /**
    *   Checks the output array to see if it matches
    *   with the list
    */
    public void testToArray() {
        list.add("A");
        list.add("B");
        String[] compare = new String[] { "A", "B" };
        Object[] listArray = list.toArray();
        for (int i = 0; i < list.getLength(); i++) {
            assertTrue(listArray[i].equals(compare[i]));
        }
    }


    /**
    *   Tests the inner iterator class
    */
    public void testIterator() {
        list.add("A");
        list.add("B");
        Iterator<String> iter = list.iterator();
        assertTrue(iter.hasNext());
        assertEquals("A", iter.next());
        assertEquals("B", iter.next());
        assertFalse(iter.hasNext());
        Exception thrown = null;
        try {
            iter.next();
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
    }


    /**
    * Tests the contains(T) and the exception
    * thrown when T is null
    */
    public void testContains() {
        Exception thrown = null;
        try {
            list.contains(null);
        }
        catch (Exception e) {
            thrown = e;
        }
        assertNotNull(thrown);
        list.add("A");
        list.add("B");
        assertTrue(list.contains("A"));
        assertTrue(list.contains("B"));
        assertFalse(list.contains("C"));
    }

}
