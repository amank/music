// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)
package prj5;

/**
 * Class to store an individual person's credentials and
 * survey responses.
 * 
 * @author Sai (saihhh)
 * @version 11/19/19
 *
 */
public class Person {
    private int id;
    private HobbyEnum hobby;
    private MajorEnum major;
    private RegionEnum region;
    private int[] responses;


    /**
     * Creates new Person object and initializes its fields.
     * 
     * @param id
     *            person identification number.
     * @param hob
     *            person's preferred hobby
     * @param maj
     *            person's preferred major
     * @param reg
     *            person's geographical region
     * @param responses
     *            person's survey responses.
     */
    public Person(
        int id,
        HobbyEnum hob,
        MajorEnum maj,
        RegionEnum reg,
        int[] responses) {
        this.id = id;
        hobby = hob;
        major = maj;
        region = reg;
        this.responses = responses;
    }


    /**
     * Returns person's identification number
     * 
     * @return integer identification number
     */
    public int getID() {
        return id;
    }


    /**
     * Returns person's hobby
     * 
     * @return HobbyEnum corresponding to person's hobby.
     */
    public HobbyEnum getHobby() {
        return hobby;
    }


    /**
     * Returns person's major
     * 
     * @return MajorEnum corresponding to person's major.
     */
    public MajorEnum getMajor() {
        return major;
    }


    /**
     * Returns person's region
     * 
     * @return RegionEnum corresponding to person's region.
     */
    public RegionEnum getRegion() {
        return region;
    }


    /**
     * Returns person's survey responses
     * 
     * @return integer array with person's survey responses.
     */
    public int[] getResponses() {
        return responses;
    }


    /**
     * Returns person's credentials as a string
     * 
     * @return String value of person's credentials.
     */
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("[");
        str.append(id);
        str.append(", ");
        str.append(hobby);
        str.append(", ");
        str.append(major);
        str.append(", ");
        str.append(region);
        str.append("]");
        return str.toString();

    }
}
