// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import java.util.Iterator;

/**
 * Class to calculate the heard and like percentages for the given
 * songs. Major calculation class.
 * 
 * @author Aman Kothari (amank)
 * @version 11/19/2019
 */
public class MusicCalculator {

    private SongList songs;
    private LinkedList<Person> people;


    /**
     * Creates new MusicCalculator object and initializes fields
     * with appropriate values.
     * 
     * @param songs
     *            SongList object containing songs.
     * @param people
     *            LinkedList object containing people and theire responses.
     */
    public MusicCalculator(SongList songs, LinkedList<Person> people) {
        this.songs = songs;
        this.people = people;
    }


    /**
     * Return SongList object containing songs.
     * 
     * @return SongList object containing songs
     */
    public SongList getSongList() {
        return songs;
    }


    /**
     * Returns LinkedList object containing people.
     * 
     * @return LinkedList object containing people
     */
    public LinkedList<Person> getPersonList() {
        return people;
    }


    /**
     * Method to calculate the heard and like percentages for each song
     * categorizing people based on their hobbies.
     * 
     * @param songId
     *            song identification number
     * @return an array of percentages
     */
    public int[] calculateHobbyForSong(int songId) {
        Iterator<Person> iter = people.iterator();
        int[] hobbyRecord = new int[8];
        int[] count = new int[] { 0, 0, 0, 0 };
        while (iter.hasNext()) {
            Person person = iter.next();
            int[] responses = person.getResponses();
            if (responses[2 * songId] != -1) {
                int enumChoice = 0;
                switch (person.getHobby()) {
                    case READ:
                        enumChoice = 0;
                        break;
                    case ART:
                        enumChoice = 2;
                        break;
                    case SPORTS:
                        enumChoice = 4;
                        break;
                    default:
                        enumChoice = 6;
                        break;

                }
                hobbyRecord[enumChoice] += responses[2 * songId];
                if (responses[2 * songId + 1] != -1) {
                    hobbyRecord[enumChoice + 1] += responses[2 * songId + 1];
                }
                count[enumChoice / 2] += 1;
            }

        }
        for (int i = 0; i < 4; i++) {
            if (count[i] != 0) {
                hobbyRecord[2 * i] = (hobbyRecord[2 * i] * 100) / count[i];
                hobbyRecord[2 * i + 1] = (hobbyRecord[2 * i + 1] * 100)
                    / count[i];
            }
            else {
                hobbyRecord[2 * i] = 0;
                hobbyRecord[2 * i + 1] = 0;
            }

        }
        return hobbyRecord;
    }


    /**
     * Method to calculate the heard and like percentages for each song
     * categorizing people based on their geographical regions.
     * 
     * @param songId
     *            song identification number
     * @return an array of percentages
     */
    public int[] calculateRegionForSong(int songId) {
        Iterator<Person> iter = people.iterator();
        int[] regionRecord = new int[8];
        int[] count = new int[] { 0, 0, 0, 0 };

        while (iter.hasNext()) {
            Person person = iter.next();
            int[] responses = person.getResponses();
            if (responses[2 * songId] != -1) {
                int enumChoice = 0;
                switch (person.getRegion()) {
                    case NE_US:
                        enumChoice = 0;
                        break;
                    case SE_US:
                        enumChoice = 2;
                        break;
                    case THE_REST_OF_US:
                        enumChoice = 4;
                        break;
                    default:
                        enumChoice = 6;
                        break;

                }
                regionRecord[enumChoice] += responses[2 * songId];
                if (responses[2 * songId + 1] != -1) {
                    regionRecord[enumChoice + 1] += responses[2 * songId + 1];
                }
                count[enumChoice / 2] += 1;
            }
        }
        for (int i = 0; i < 4; i++) {
            if (count[i] != 0) {
                regionRecord[2 * i] = (regionRecord[2 * i] * 100) / count[i];
                regionRecord[2 * i + 1] = (regionRecord[2 * i + 1] * 100)
                    / count[i];
            }
            else {
                regionRecord[2 * i] = 0;
                regionRecord[2 * i + 1] = 0;
            }

        }
        return regionRecord;
    }


    /**
     * Method to calculate the heard and like percentages for each song
     * categorizing people based on their majors.
     * 
     * @param songId
     *            song identification number
     * @return an array of percentages
     */
    public int[] calculateMajorForSong(int songId) {
        Iterator<Person> iter = people.iterator();
        int[] majorRecord = new int[8];
        int[] count = new int[] { 0, 0, 0, 0 };

        while (iter.hasNext()) {
            Person person = iter.next();
            int[] responses = person.getResponses();
            if (responses[2 * songId] != -1) {
                int enumChoice = 0;
                switch (person.getMajor()) {
                    case CS:
                        enumChoice = 0;
                        break;
                    case MATH_CMDA:
                        enumChoice = 2;
                        break;
                    case ENGINEERING:
                        enumChoice = 4;
                        break;
                    default:
                        enumChoice = 6;
                        break;

                }
                majorRecord[enumChoice] += responses[2 * songId];
                if (responses[2 * songId + 1] != -1) {
                    majorRecord[enumChoice + 1] += responses[2 * songId + 1];
                }
                count[enumChoice / 2] += 1;
            }
        }
        for (int i = 0; i < 4; i++) {
            if (count[i] != 0) {
                majorRecord[2 * i] = (majorRecord[2 * i] * 100) / count[i];
                majorRecord[2 * i + 1] = (majorRecord[2 * i + 1] * 100)
                    / count[i];
            }
            else {
                majorRecord[2 * i] = 0;
                majorRecord[2 * i + 1] = 0;
            }

        }
        return majorRecord;
    }

}
