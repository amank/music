// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Aman Kothari (amank)

package prj5;

import java.io.FileNotFoundException;

/**
 * Input() class integrates all code to run as an application.
 * 
 * 
 * @author Aman Kothari (amank)
 * @version 11/19/19
 */
public class Input {

    /**
     * Main method that executes when the application
     * is run.
     * 
     * @param args
     *            takes a string argument to be read from;
     *            gives file names to be read into the application.
     * @throws FileNotFoundException
     *             when either file is not found.
     */

    public static void main(String[] args) throws FileNotFoundException {
        @SuppressWarnings("unused")
        FileReader reader = new FileReader(args[1], args[0]);
    }
}
